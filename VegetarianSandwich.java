public abstract class VegetarianSandwich implements Sandwich {
    private String filling;
    public String getFilling(){
        return this.filling;
    }
    public void addFilling(String topping){
        String[] array = {"chicken","beef","fish","meat","pork"};
        for(int i=0; i < array.length; i++){
            if(topping == array[i]){
                throw new IllegalArgumentException("Wrong type of filling!");
            }
        }
        this.filling = topping;
    }
    public final boolean isVegetarian(){
        return true;
    }
    public boolean isVegan(){
        String[] array = {"cheese","egg"};
        for(int i=0; i < array.length; i++){
            if(this.filling == array[i]){
        return false;
            }
        }
    return true;
    }
    public abstract String addProtein();
}