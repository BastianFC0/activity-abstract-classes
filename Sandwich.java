public interface Sandwich{
    String getFilling();
    void addFilling(String topping);
    boolean isVegetarian();
}