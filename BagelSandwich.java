public class BagelSandwich implements Sandwich {
    String filling;
    public BagelSandwich(){
        this.filling = "";
    }
    public void addFilling(String topping){
        this.filling = topping;
    }
    public String getFilling(){
        return this.filling;
    }
    public boolean isVegetarian(){
        throw new UnsupportedOperationException("Error");
    }
}
