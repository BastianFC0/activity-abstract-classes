public class Application {
    public static void main(String[] args){
        Sandwich s = new TofuSandwich();
        VegetarianSandwich v = ((VegetarianSandwich)s);
        v.isVegan();
        VegetarianSandwich c = new CucumberSandwich();
        System.out.println(c.isVegetarian());
    }
}
